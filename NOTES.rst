==================================================
WebGL frameworks
==================================================

Список доступен тут:
https://en.wikipedia.org/wiki/List_of_WebGL_frameworks
https://medium.com/alistapart/all-the-webgl-tools-and-frameworks-i-have-looked-at-a1c22154591b

Рассматриваем фреймворки с открытой лицензией MIT, Apache, BSD
*с поддержкой физ движка*.
                                           Git
* Away3D                          |        671 - версия под flash
  https://github.com/awayjs/core  |        52 - позволяет собирать под android
* Babylon.js                      |        6529
* CopperLicht                     |         -
* SceneJS                         |        573
* Whitestorm.js                   |        4774
Дополнительно:                    |
* luma.gl -- data visualization   |        1004
* Three.js                        |        41394

*pixi.js -- самый быстрый для 2d  |        18601


whiteshorm, так как под идее должен поддерживаться
функционал Three.js + как минимум физика.

* https://whs.io/ -- документация
* https://whs-dev.surge.sh/examples/ -- различные примеры
* http://undeclaredvariable.com/whitestormjs-vs-three-js/ -- пример с описанием и сравнение
* https://github.com/WhitestormJS
* https://threejs.org
* https://github.com/mrdoob/three.js/

Еще один список:

* https://gist.github.com/dmnsgn/76878ba6903cf15789b712464875cfdc

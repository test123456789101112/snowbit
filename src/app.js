(function(){
    const app = new WHS.App([
        new WHS.ElementModule(),
        new WHS.SceneModule(),
        new WHS.DefineModule('camera', new WHS.PerspectiveCamera(UTILS.appDefaults.camera)),
        new WHS.RenderingModule(UTILS.appDefaults.rendering, {
            shadow: true
        }),
        //new PHYSICS.WorldModule(UTILS.appDefaults.physics),
        new WHS.OrbitControlsModule(),
        new WHS.ResizeModule(),
        new StatsModule()
    ]);

    const sphere = new WHS.Sphere({ // Create sphere comonent.
        geometry: {
            radius: 5,
            widthSegments: 32,
            heightSegments: 32
        },

        modules: [
            new PHYSICS.SphereModule({
                mass: 10,
                restitution: 1
            })
        ],

        material: new THREE.MeshPhongMaterial({
            color: UTILS.$colors.mesh
        }),

        position: new THREE.Vector3(0, 20, 0)
    });

    sphere.addTo(app);

    UTILS.addBoxPlane(app);
    UTILS.addBasicLights(app);

    app.start(); // Start animations and physics simulation.
})();

// (function(){
//     const app = new WHS.App([
//         new WHS.ElementModule(), // Apply to DOM.
//         new WHS.SceneModule(), // Create a new THREE.Scene and set it to app.

//         new WHS.DefineModule('camera', new WHS.PerspectiveCamera({ // Apply a camera.
//             position: new THREE.Vector3(0, 0, 50)
//         })),

//         new WHS.RenderingModule({bgColor: 0x162129}), // Apply THREE.WebGLRenderer
//         new WHS.ResizeModule() // Make it resizable.
//     ]);

//     app.start(); // Run app.
// })();
